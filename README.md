# Kbd backlight

CLI: start as `./kbd_backlight.sh`
GUI: start as `./kbd_backlight.sh --gui`

Fast and effortless to set keyboard backlight brightness (on ASUS laptops for now).

[!] No external dependencies needed (only `sudo` and `zenity` - for gui).
