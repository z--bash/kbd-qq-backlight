#!/bin/sh

# to run me without root do this once as root:
#   sudo su -c "echo -en 'ALL ALL=(ALL) NOPASSWD: /usr/bin/tee /sys/class/leds/asus\\:\\:kbd_backlight/brightness\n' > /etc/sudoers.d/kbd_backlight && chmod 0440 /etc/sudoers.d/kbd_backlight"

LABEL="Keyboard brightness control"
DES_0="OFF"
DES_1="dim"
DES_2="bright"
DES_3="very bright"
DES_v="view current and quit"

MODE="v"
GUI=false

CURRENT=$(cat /sys/class/leds/asus::kbd_backlight/brightness)

cli_view_current()
{
    echo "Current keyboard brightness is $CURRENT"
}

cli_view_help()
{
    echo $LABEL
    echo ""
    echo "0 - $DES_0"
    echo "1 - $DES_1"
    echo "2 - $DES_2"
    echo "3 - $DES_3"
    echo "v - $DES_v"
    echo ""
}

if [ -z "$1" ]; then
    cli_view_help
else
    if [ "$1" == "-1" ]; then
        cli_view_current
        exit 0
    elif [ "$1" == "--help" ]; then
        cli_view_help
        exit 0
    elif [ "$1" == "--gui" ]; then
        GUI=true
    else
        USER_MODE=$1
    fi
fi

set_mode()
{
    if [ "$USER_MODE" == "v" ]; then
        return
    fi

    case $USER_MODE in
        ''|*[!0-3]*) echo "Invalid option. Keeping current brightness"; return ;;
        *) : ;;
    esac

    if [ "$USER_MODE" -ge 0 ] && [ "$USER_MODE" -le 3 ]; then
        MODE=$USER_MODE
    fi
}

cli_run()
{
	printf "Setting brightness to "
	echo $MODE | sudo /usr/bin/tee /sys/class/leds/asus::kbd_backlight/brightness
}

###

if [ "$GUI" = true ]; then
    gui_ask_quest()
    {
	[ "$CURRENT" == "0" ] && is_current_0=TRUE || is_current_0=FALSE
	[ "$CURRENT" == "1" ] && is_current_1=TRUE || is_current_1=FALSE
	[ "$CURRENT" == "2" ] && is_current_2=TRUE || is_current_2=FALSE
	[ "$CURRENT" == "3" ] && is_current_3=TRUE || is_current_3=FALSE

        USER_MODE=$(echo $(zenity \
                --list --radiolist --title="$LABEL" --width=345 --height=210 \
                --column="#" --column="Description" \
                    $is_current_0 "0 - $DES_0" \
                    $is_current_1 "1 - $DES_1" \
                    $is_current_2 "2 - $DES_2" \
                    $is_current_3 "3 - $DES_3" ) | cut -c1-1)
    }

    gui_ask_pass()
    {
        echo $(zenity --password --title="Authorise keyboard brightness access")
    }

    gui_run()
    {
	# first try passwordless approach
	cli_run || \
        { echo $(gui_ask_pass); echo $MODE; } | sudo -k -S /usr/bin/tee /sys/class/leds/asus::kbd_backlight/brightness
    }

    gui_ask_quest
    set_mode
    gui_run
else
    cli_ask_quest()
    {
        read USER_M
        USER_MODE=$USER_M
    }

    if [ -z "$USER_MODE" ]; then
        printf "Select value [V/0/1/2/3]: "
        cli_ask_quest
    fi

    set_mode

    if [ "$MODE" == "v" ]; then
        cli_view_current
    else
        cli_run
    fi
fi
